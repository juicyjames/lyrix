import os
import sys
import json

import lyricsgenius as lg
import spotipy
from dotenv import load_dotenv

# load envirooment variables
# TODO:
# reset secrets
load_dotenv()
spotify_client_id = os.environ['SPOTIFY_CLIENT_ID']
spotify_secret = os.environ['SPOTIFY_CLIENT_SECRET']
spotify_redirect_uri = os.environ['SPOTIFY_REDIRECT_URI']
genius_access_token = os.environ['GENIUS_ACCESS_TOKEN']

scope = 'user-read-currently-playing'

# oauth object
oauth_object = spotipy.SpotifyOAuth(client_id=spotify_client_id,
                                    client_secret=spotify_secret,
                                    redirect_uri=spotify_redirect_uri,
                                    scope=scope)

# get_access_token() is deprecated
#token_dict = oauth_object.get_access_token()
# use get_cached_token() instead
token_dict = oauth_object.get_cached_token()
token = token_dict['access_token']

# spotify object
spotify_object = spotipy.Spotify(auth=token)

# genius object
genius_object = lg.Genius(genius_access_token, verbose=False)
genius_object.response_format = 'plain'
genius_object.remove_section_headers = True
genius_object.song

class SongInfo:
    ''' this class is for getting current track info from spotify '''
    def __init__(self):
        self.track = spotify_object.currently_playing()
        self.genius = genius_object

    def song_title(self):
        ''' get the name of the currently playing track '''
        song_title = self.track['item']['name']
        return song_title

    def artist_name(self):
        ''' get artist name '''
        artist_name = self.track['item']['album']['artists'][0]['name']
        return artist_name

    def track_status(self):
        ''' check to see if a song or add is playing '''
        status = self.track['currently_playing_type']
        return status

    def track_progress(self):
        ''' current track progress '''
        progress = self.track['progress_ms']
        return progress

    def get_lyrics(self):
        ''' get lyrics for the current song '''
        # get lyrics from genius api return a dictionary
        song = self.genius.search_song(title=self.song_title(), artist=self.artist_name(), get_full_info=False)
        # print lyrics if there are any
        if song == None:
            print('There are no lyrics for:', self.song_title(), 'by', self.artist_name())
        else:
            print(song.lyrics)

    def dumpJson(self):
        ''' print spotify json response '''
        print(json.dumps(self.track, sort_keys=False, indent=2))

def main():
    song = SongInfo()
    song.get_lyrics()
    # dump json
    # song.dumpJson()

if __name__ == "__main__":
    try:
        main()
    except KeyboardInterrupt:
        print('\n' + 'Interrupted')
        sys.exit(0)
