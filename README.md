# Lyrix
a program for getting the lyrics for the currently playing song from spotify.

# Setup
add the following to .env file in root directory

* SPOTIFY_CLIENT_ID = **'your-spotify-client-id'**
* SPOTIFY_CLIENT_SECRET = **'your-spotify-client-secret'**
* SPOTIFY_REDIRECT_URI = **'http://localhost:8888/callback'**
* GENIUS_ACCESS_TOKEN = **'your-genius-access-token'**

# Notes
only works with official spotify client
